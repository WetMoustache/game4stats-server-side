package es.wetware.game4statisticsserver.controller;
//import javax.validation.Valid;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import Poster.Post2Gmc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import es.wetware.game4statisticsserver.dao.GameDAO;
import es.wetware.game4statisticsserver.dao.MatchDAO;
import es.wetware.game4statisticsserver.dao.PlayerDAO;
import es.wetware.game4statisticsserver.dao.PlayerDAOImpl;
import es.wetware.game4statisticsserver.dao.PlayerScoreDAO;
import es.wetware.game4statisticsserver.model.Game;
import es.wetware.game4statisticsserver.model.Match;
import es.wetware.game4statisticsserver.model.Player;
import es.wetware.game4statisticsserver.model.PlayersForMatch;
import es.wetware.game4statisticsserver.util.Config;
import es.wetware.game4statisticsserver.util.JwtUtil;
import es.wetware.game4statisticsserver.util.jsonUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;


@Controller
@SessionAttributes("playerBean")
public class PlayerController {
	
	ServletContext servletContext;
	@Autowired
	private PlayerDAO playerDAO;
	@Autowired
	private GameDAO gameDAO;
	@Autowired
	private MatchDAO matchDAO;
	@Autowired
	private PlayerScoreDAO playerScoreDAO;
	
	@Autowired
	public PlayerController (ServletContext context){
		this.servletContext = context;
	}
	/****************************************
	 * 
	 * 			
	 * 			   POST METHODS
	 * 
	 * 
	 *****************************************/
	/*@RequestMapping(method=RequestMethod.POST)
	public String processSubmit(Player userBean, Model model) {
			userBean.setName(userBean.getName());
			playerDAO.add(userBean);
			model.addAttribute("users", playerDAO.getAll());
			return "users/list";	
	}*/
	
	@RequestMapping (value = "/Login", method=RequestMethod.POST)
	public String login (HttpServletRequest request, HttpServletResponse response){
		Player player = null;
		System.out.println("Register");
		try {
			Player userPlayer = jsonUtil.jsonToPlayer(request.getInputStream());
			player = playerDAO.getByNamePassword(userPlayer.getName(), userPlayer.getPassword());
			if(player == null){
				return "noMatchNamePassword";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return player.getToken();
	}
	
	@RequestMapping(value = "/Register", method = RequestMethod.POST)
	public @ResponseBody String processRegister(HttpServletRequest request, HttpServletResponse response) {
		String sqlResult = null;
		String responseMessage = null;
		System.out.println("Register");
		try {
			Player userPlayer = jsonUtil.jsonToPlayer(request.getInputStream());
			sqlResult = playerDAO.add(userPlayer);
			String token = generateToken(userPlayer);
			userPlayer.setToken(token);
			responseMessage = generateResponseRegister(sqlResult, userPlayer);
			
			Post2Gmc.sendMessage(Config.getAPIKEY(), userPlayer.getGcmId());
			
		} catch (IOException e) {
			e.printStackTrace(); 
		} 
		return responseMessage;
	}
	
	@RequestMapping(value = "/GetUser", method = RequestMethod.POST)
	public @ResponseBody String processGetUser(HttpServletRequest request, HttpServletResponse response) {
		String responseMessage;
		String token = request.getHeader("Token");
		String userName = parseJWT(token);
		if(userName != null){
			Player user = playerDAO.getByName(userName);
			if(user != null){
				user = user.getUserPlayer(user);
				responseMessage = generateResponseRegister("success", user);
			}
			else
				responseMessage = "noMatchFound";
			return responseMessage;
		}
		return "noMatchFound";
	}
	
	@RequestMapping(value = "/GetPlayers", method = RequestMethod.POST)
	protected void getPlayers(HttpServletRequest request, HttpServletResponse response) {
		String token;
		if((token = parseJWT(request.getHeader("Token")))!=null){
			OutputStream jsonResponse = null;				
			ArrayList<Player> playerList = playerDAO.getAll();
			jsonResponse = jsonUtil.playersArrayToJson(playerList);

			response.setContentType("accept/json");
			try {
				response.getOutputStream().print(jsonResponse.toString());
				response.getOutputStream().flush();
				response.getOutputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}
	
	@RequestMapping(value = "/GetGames", method = RequestMethod.POST)
	protected void getGames(HttpServletRequest request, HttpServletResponse response) {
		OutputStream jsonResponse = null;
		String token;
		if((token = parseJWT(request.getHeader("Token")))!=null){
			ArrayList<Game> gameList = gameDAO.getAll();
			jsonResponse = jsonUtil.gamesArrayToJson(gameList);
			response.setContentType("accept/json");
			try {
				response.getOutputStream().print(jsonResponse.toString());
				response.getOutputStream().flush();
				response.getOutputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value = "/PostMatch", method = RequestMethod.POST)
	protected void postMatch(HttpServletRequest request, HttpServletResponse response) {
		OutputStream jsonResponse = null;
		String token;
		if((token = parseJWT(request.getHeader("Token")))!=null){
			try {
				
				PlayersForMatch playersForMatch = jsonUtil.jsonToPlayersForMatch(request.getInputStream());
				Player player = playerDAO.getByName(playersForMatch.getCreatorName());
			
				provisionalGameProtocol(playersForMatch, player.getIdPlayer());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value = "/PostMatch/Accept", method = RequestMethod.POST)
	protected void postMatchAccept(HttpServletRequest request, HttpServletResponse response) {
		OutputStream jsonResponse = null;
		String user;
		if((user = parseJWT(request.getHeader("Token")))!=null){
			try {
				PlayersForMatch playersForMatch = jsonUtil.jsonToPlayersForMatch(request.getInputStream());
				agreedProtocol(playersForMatch, user, playersForMatch.getCreatorName(), playersForMatch.getTimestamp());
				/*response.setContentType("accept/json");
				response.getOutputStream().print(jsonResponse.toString());
				response.getOutputStream().flush();
				response.getOutputStream().close();*/
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value = "/PostMatch/Reject", method = RequestMethod.POST)
	protected void postMatchReject(HttpServletRequest request, HttpServletResponse response) {
		OutputStream jsonResponse = null;
		String user;
		if((user = parseJWT(request.getHeader("Token")))!=null){
			try {
				
				PlayersForMatch playersForMatch = jsonUtil.jsonToPlayersForMatch(request.getInputStream());
				disagreedProtocol(playersForMatch, user);
				//provisionalGameProtocol(playersForMatch);
				
				/*response.setContentType("accept/json");
				response.getOutputStream().print(jsonResponse.toString());
				response.getOutputStream().flush();
				response.getOutputStream().close();*/
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	

	/****************************************
	 * 
	 * 			
	 * 			   GET METHODS
	 * 
	 * 
	 *****************************************/
	
	public @ResponseBody String byParameter(@RequestParam("foo") String foo) {
	    return "Mapped by path + method + presence of query parameter! (MappingController) - foo = "
	           + foo;
	}
	
	private String generateResponseRegister(String sqlResult, Player player){
		if(sqlResult == "success"){
			Player user = new Player();
			user = user.getUserPlayer(playerDAO.getByName(player.getName()));
			return jsonUtil.playerToJson(user);
		}
		else
			return sqlResult;	//sqlResult-->"duplicated"
	}
	
	private String generateToken(Player player) {
		JwtUtil jwtUtil = new JwtUtil();
		String token = Jwts.builder().setSubject(player.getName()).setSubject( player.getPassword()).signWith(SignatureAlgorithm.HS512, Config.getAPIKEY()).compact();
		String result = playerDAO.assignToken(player.getName(), token);
		if(result == "ok")
			return token;
		else
			return "failure";
	}
	
	private String parseJWT(String jwt){
		try {
			
			String key = Config.getAPIKEY();
		    Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(jwt).getBody();
		    String subject = claims.getSubject();
		    System.out.println("USER: " + subject);
		    //OK, we can trust this JWT
		    return subject;
		} catch (SignatureException e) {
		    return null;
		}
	}

	private String getKey(String userName) {
		return playerDAO.getKey(userName);
	}
	
	private void provisionalGameProtocol(PlayersForMatch playersForMatch, Long idCreator) {
		Long idJuego = getIdJuego(playersForMatch.getGame().getNombre());
		ArrayList<String> regIdList = getRegIdList(playersForMatch.getPlayerList());
		ArrayList<String> nameList = getNameList(playersForMatch.getPlayerList());
		java.sql.Timestamp  sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());
		Player player = playerDAO.getById(idCreator);
		
		matchDAO.addProvisionalMatch(idJuego, playersForMatch,idCreator, sqlDate);
		//String jsonMessage = jsonUtil.playersForMatchToJson(playersForMatch);
		Post2Gmc.sendMessage(Config.getAPIKEY(), regIdList, nameList , playersForMatch.getPlayerScore(), idJuego, sqlDate.toString(), player.getName());
	}
	
	private ArrayList<String> getNameList(ArrayList<Player> playerList) {
		ArrayList<String> nameList = new ArrayList<>();
		for(int i =0; i<playerList.size(); i++){
			Player p = playerDAO.getByName(playerList.get(i).getName());
			nameList.add(p.getName());
		}
		return nameList;
	}
	private Long getIdJuego(String nombre) {
		return gameDAO.getByName(nombre).getId();
	}
	
	private ArrayList<String> getRegIdList(ArrayList<Player> playerList){
		ArrayList<String> regIdList = new ArrayList<>();
		for(int i =0; i<playerList.size(); i++){
			Player p = playerDAO.getByName(playerList.get(i).getName());
			regIdList.add(p.getGcmId());
		}
		return regIdList;
	}
	
	private void agreedProtocol(PlayersForMatch playersForMatch, String userName, String creatorName, String timestamp) {
		boolean everyoneAgreed = false;
		Player player = playerDAO.getByName(userName);
		Player creator = playerDAO.getByName(creatorName);
		everyoneAgreed = matchDAO.updateProvisional(playersForMatch.getGame().getId(), player.getIdPlayer(), creator.getIdPlayer(), timestamp);
		if(everyoneAgreed)
			recordScore(playersForMatch, timestamp);
		
	}
	
	private void disagreedProtocol(PlayersForMatch playersForMatch, String userName) {
		rejectMatchProposal(userName, playersForMatch);
	}
	
	private void rejectMatchProposal(String userName, PlayersForMatch playersForMatch) {
		ArrayList<String> regIdList = getRegIdList(playersForMatch.getPlayerList());
		matchDAO.deleteMatchById(playersForMatch.getGame().getId());
		//String jsonMessage = jsonUtil.playersForMatchToJson(playersForMatch);
		Post2Gmc.sendMatchCanceledMessage(Config.getAPIKEY(), regIdList, userName);
	}
	
	private void recordScore(PlayersForMatch playersForMatch, String timestamp) {
		for(int i=0; i<playersForMatch.getPlayerList().size(); i++){
			Player player = playerDAO.getByName(playersForMatch.getPlayerList().get(i).getName());
			Match match = matchDAO.getByCreatorTimestamp(playersForMatch.getCreatorName(), playersForMatch.getTimestamp());
			playerScoreDAO.add(player.getIdPlayer(), match.getIdParida(), playersForMatch.getPlayerScore().get(i));
		}
	}
}

