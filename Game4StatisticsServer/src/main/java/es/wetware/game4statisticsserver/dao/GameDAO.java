package es.wetware.game4statisticsserver.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.wetware.game4statisticsserver.model.Game;

@Repository
public interface GameDAO {
	public ArrayList<Game> getAll();
	public Game getByName(String name);
}
