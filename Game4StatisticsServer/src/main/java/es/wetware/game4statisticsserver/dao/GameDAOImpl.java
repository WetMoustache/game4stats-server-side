package es.wetware.game4statisticsserver.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import es.wetware.game4statisticsserver.model.Game;
import es.wetware.game4statisticsserver.model.Match;
import es.wetware.game4statisticsserver.model.jdbc.JdbcConnection;

@Service
public class GameDAOImpl implements GameDAO{
	List<Game> games;
	JdbcConnection jdbcBind;
	
	public GameDAOImpl(){
		jdbcBind = new JdbcConnection();
	}
	
	@Override
	public ArrayList<Game> getAll() {
		Statement ps;
		String query;
		ResultSet rset;
		ArrayList<Game> games = new ArrayList<Game>();
		try{
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT nombre FROM juego";
			rset = ps.executeQuery(query);
			while (rset.next()){
				Game g = new Game();
				g.setNombre(rset.getString(1));
				games.add(g);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		jdbcBind.dbDisconnect();
		return games;
	}
	
	@Override
	public Game getByName(String name){
		Statement ps;
		String query;
		ResultSet rset;
		Game g = null;
		try{
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT * FROM juego where nombre LIKE '"+ name + "'";
			rset = ps.executeQuery(query);
			while (rset.next()){
				g = new Game();
				g.setId(rset.getLong(1));
				g.setNombre(rset.getString(2));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		jdbcBind.dbDisconnect();
		return g;
	}
}
