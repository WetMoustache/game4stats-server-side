package es.wetware.game4statisticsserver.dao;

import java.sql.Timestamp;

import org.springframework.stereotype.Repository;

import es.wetware.game4statisticsserver.model.Match;
import es.wetware.game4statisticsserver.model.PlayersForMatch;

@Repository
public interface MatchDAO {
	void addProvisionalMatch(Long gameId, PlayersForMatch playersForMatch, Long idCreador, Timestamp sqlDate);
	boolean updateProvisional(Long idGame, Long idPlayer, Long idCreator, String timestamp);
	void deleteMatchById(Long id);
	Match getByCreatorTimestamp(String creator, String timestamp);
}
