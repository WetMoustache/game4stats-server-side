package es.wetware.game4statisticsserver.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.springframework.stereotype.Service;

import es.wetware.game4statisticsserver.model.Match;
import es.wetware.game4statisticsserver.model.Player;
import es.wetware.game4statisticsserver.model.PlayersForMatch;
import es.wetware.game4statisticsserver.model.jdbc.JdbcConnection;
@Service
public class MatchDAOImpl implements MatchDAO{
	
	JdbcConnection jdbcBind;

	public MatchDAOImpl(){
		jdbcBind = new JdbcConnection();
	}
	
	@Override
	public void addProvisionalMatch(Long gameId, PlayersForMatch playersForMatch, Long idCreador, Timestamp sqlDate ) {
		Statement ps;
		String query;
		Long idJuego;
		int numeroJugadores;
		idJuego = gameId;
		numeroJugadores = getPlayerNumber(playersForMatch);
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "INSERT INTO partida (idjuego, numerojugadores, estado, creator, creationtime) VALUES ("+idJuego+","+numeroJugadores+",'"+"pending"+"' ," + idCreador +",'"+ sqlDate+ "')";
			System.out.println(query);
			ps.execute(query);
			jdbcBind.dbDisconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean updateProvisional(Long idGame, Long idPlayer, Long idCreator, String timestamp) {
		Statement ps;
		String query;
		ResultSet rset;
		boolean everyoneAgreed = false;
		Match m = null;
		try{
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT * FROM partida where (creator = "+ idCreator + " AND " + "creationtime = '" + timestamp +"');";
			System.out.println(query);
			rset = ps.executeQuery(query);
			while (rset.next()){
				m = new Match();
				m.setIdParida(rset.getLong(1));
				m.setIdJuego(rset.getLong(2));
				m.setNoPlayers(rset.getInt(3));
				m.setEstado(rset.getString(4));
				m.setIdJugador1(rset.getLong(5));
				m.setIdJugador2(rset.getLong(6));
				m.setIdJugador3(rset.getLong(7));
				m.setIdJugador4(rset.getLong(8));
				m.setIdJugador5(rset.getLong(9));
				m.setIdJugador6(rset.getLong(10));
				m.setIdJugador7(rset.getLong(11));
				m.setIdJugador8(rset.getLong(12));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		jdbcBind.dbDisconnect();
		int pos = m.assignNextPlayer(idPlayer);
		
		jdbcBind.dbConnect();
		try {
			ps = jdbcBind.getConnection().createStatement();
			query = "UPDATE partida SET idjugador" + pos +" = " + idPlayer + " WHERE (creator = "+ idCreator + " AND " + "creationtime = '" + timestamp +"');";
			System.out.println(query);
			rset = ps.executeQuery(query);
			if(m.getNoPlayers()==pos){
				query = "UPDATE partida SET estado = 'accepted'";
				everyoneAgreed = true;
				rset = ps.executeQuery(query);	
			}
			jdbcBind.dbDisconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(everyoneAgreed)
			return true;
		return false;
	}
	
	@Override
	public void deleteMatchById(Long id){
		Statement ps;
		String query;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "DELETE FROM partida WHERE idpartida = "+id;
			System.out.println(query);
			ps.execute(query);
			jdbcBind.dbDisconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	private int getPlayerNumber(PlayersForMatch playersForMatch){
		return playersForMatch.getPlayerList().size();
	}

	@Override
	public Match getByCreatorTimestamp(String creator, String timestamp) {
		Statement ps;
		String query;
		Match match = null;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT * FROM partida WHERE (creator LIKE '"+creator+"' AND creationtime = '"+timestamp+"'";
			System.out.println(query);
			ResultSet rs = ps.executeQuery(query);
			while(rs.next()){
				match = new Match(rs.getLong("idpartida"), rs.getLong("idjuego"), rs.getInt("numerojugadores"), rs.getString("estado"), rs.getLong("idjugador1"), rs.getLong("idjugador2"), rs.getLong("idjugador3"), rs.getLong("idjugador4"), rs.getLong("idjugador5"), rs.getLong("idjugador6"), rs.getLong("idjugador7"), rs.getLong("idjugador8"), rs.getInt("creator"), rs.getString("creationtime"));
			}
			jdbcBind.dbDisconnect();
			return match;
		} catch (SQLException e) {
			return null;
		}
	}
	
	
}
