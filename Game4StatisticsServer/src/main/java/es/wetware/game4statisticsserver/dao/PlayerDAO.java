package es.wetware.game4statisticsserver.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.wetware.game4statisticsserver.model.Player;

@Repository
public interface PlayerDAO {
	public ArrayList<Player> getAll();
	public Player getByNamePassword(String username, String password);
	public Player getByName(String username);
	public String add(Player user);
	public String assignToken(String name, String token);
	public String getKey(String name);
	public String getGcmId();
	public Player getById(Long idCreator);
}