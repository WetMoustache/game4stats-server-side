package es.wetware.game4statisticsserver.dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import es.wetware.game4statisticsserver.model.Player;
import es.wetware.game4statisticsserver.model.jdbc.JdbcConnection;

@Service
public class PlayerDAOImpl implements PlayerDAO {

	List<Player> players;
	JdbcConnection jdbcBind;
	
	public PlayerDAOImpl(){
		jdbcBind = new JdbcConnection();
	}
	
	@Override
	public ArrayList<Player> getAll() {
		Statement ps;
		String query;
		ResultSet rset;
		ArrayList<Player> players = new ArrayList<Player>();
		try{
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT nombre, foto FROM jugador";
			rset = ps.executeQuery(query);
			while (rset.next()){
				Player p = new Player();
				p.setName(rset.getString(1));
				p.setEncodedImage(rset.getBytes(2));
				players.add(p);
				//PreparedStatement prepareds = jdbcBind.getConnection().prepareStatement("SELECT foto FROM jugador WHERE nombre = ?");
				//prepareds.setString(1, "Peter");
				//ResultSet rs = prepareds.executeQuery();
				//while (rs.next()) {
				//    imgBytes = rs.getBytes(1);
				//}
				//players.get(players.size()-1).setEncodedImage(imgBytes);
				//rs.close();
				//ps.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		jdbcBind.dbDisconnect();
		return players;
	}

	@Override
	public String add(Player player) {
		Statement ps;
		String query;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "INSERT INTO jugador (nombre, password, email, gcmid) VALUES ('"+player.getName()+"','"+player.getPassword()+"','"+player.getEmail()+"','"+player.getGcmId()+"')";
			System.out.println(query);
			ps.execute(query);
			jdbcBind.dbDisconnect();
			return "success";
		} catch (SQLException e) {
			switch(e.getSQLState()){
				case ("23505"):
					return "duplicated";
			}
			return "strangeBehaivour";
		}
	}

	@Override
	public Player getByNamePassword(String username, String password) {
		Statement ps;
		String query;
		Player player = null;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT * FROM jugador WHERE nombre LIKE '"+username+"' AND password LIKE '"+password+"'";
			System.out.println(query);
			ResultSet rs = ps.executeQuery(query);
			while(rs.next()){
				player = new Player(rs.getLong("idjugador"), rs.getString("nombre"), rs.getString("password"), rs.getString("email"), rs.getBytes("foto"), rs.getString("token"), rs.getString("gcmid"));
			}
			jdbcBind.dbDisconnect();
			return player;
		} catch (SQLException e) {
			return null;
		}
	}
	
	@Override
	public Player getByName(String username) {
		Statement ps;
		String query;
		Player player = null;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT * FROM jugador WHERE nombre LIKE '"+username+"'";
			System.out.println(query);
			ResultSet rs = ps.executeQuery(query);
			while(rs.next()){
				player = new Player(rs.getLong("idjugador"), rs.getString("nombre"), rs.getString("password"), rs.getString("email"), rs.getBytes("foto"), rs.getString("token"), rs.getString("gcmid"));
			}
			jdbcBind.dbDisconnect();
			return player;
		} catch (SQLException e) {
			return null;
		}
	}
	
	@Override
	public String assignToken(String name, String token){
		Statement ps;
		String query;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "UPDATE jugador SET token = " + "'" + token + "'" + " WHERE nombre LIKE " + "'" + name + "'";
			System.out.println(query);
			ps.execute(query);
			jdbcBind.dbDisconnect();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage();
		}
	}
	
	@Override
	public Player getById(Long idCreator){
		Statement ps;
		String query;
		Player player = null;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "SELECT * FROM jugador WHERE idjugador = "+idCreator;
			System.out.println(query);
			ResultSet rs = ps.executeQuery(query);
			while(rs.next()){
				player = new Player(rs.getLong("idjugador"), rs.getString("nombre"), rs.getString("password"), rs.getString("email"), rs.getBytes("foto"), rs.getString("token"), rs.getString("gcmid"));
			}
			jdbcBind.dbDisconnect();
			return player;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public String getKey(String name) {
		return null;
	}
	
	@Override
	public String getGcmId(){
		// TODO Auto-generated method stub
		return null;
	}

}
