package es.wetware.game4statisticsserver.dao;

import org.springframework.stereotype.Repository;

@Repository
public interface PlayerScoreDAO {
	void add(Long idPlayer, Long idMatch, int playerScore);
}
