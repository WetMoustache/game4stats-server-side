package es.wetware.game4statisticsserver.dao;

import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Service;

import es.wetware.game4statisticsserver.model.jdbc.JdbcConnection;

@Service
public class PlayerScoreDAOImpl implements PlayerScoreDAO{
	JdbcConnection jdbcBind;
	
	public PlayerScoreDAOImpl(){
		jdbcBind = new JdbcConnection();
	}
	@Override
	public void add(Long idPlayer, Long idMatch, int playerScore) {
		Statement ps;
		String query;
		try {
			jdbcBind.dbConnect();
			ps = jdbcBind.getConnection().createStatement();
			query = "INSERT INTO resultadosjugador (idjugador, idpartida, puntosjugador) VALUES ("+idPlayer+","+idMatch+","+playerScore+")";
			System.out.println(query);
			ps.execute(query);
			jdbcBind.dbDisconnect();
		} catch (SQLException e) {
			e.getMessage();
		}
	}

}
