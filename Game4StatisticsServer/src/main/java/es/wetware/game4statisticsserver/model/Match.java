package es.wetware.game4statisticsserver.model;

public class Match {
	Long idPartida;
	Long idJuego;
	int noPlayers;
	String estado;
	Long idJugador1;
	Long idJugador2;
	Long idJugador3;
	Long idJugador4;
	Long idJugador5;
	Long idJugador6;
	Long idJugador7;
	Long idJugador8;
	int creator;
	String creationtime;

	public Match(Long idParida, Long idJuego, int noPlayers, String estado, Long idJugador1, Long idJugador2,
			Long idJugador3, Long idJugador4, Long idJugador5, Long idJugador6, Long idJugador7, Long idJugador8, int creator, String creationtime) {
		super();
		this.idPartida = idParida;
		this.idJuego = idJuego;
		this.noPlayers = noPlayers;
		this.estado = estado;
		this.idJugador1 = idJugador1;
		this.idJugador2 = idJugador2;
		this.idJugador3 = idJugador3;
		this.idJugador4 = idJugador4;
		this.idJugador5 = idJugador5;
		this.idJugador6 = idJugador6;
		this.idJugador7 = idJugador7;
		this.idJugador8 = idJugador8;
		this.creator = creator;
		this.creationtime = creationtime;
	}
	
	public Match() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdParida() {
		return idPartida;
	}
	public void setIdParida(Long idParida) {
		this.idPartida = idParida;
	}
	public Long getIdJuego() {
		return idJuego;
	}
	public void setIdJuego(Long idJuego) {
		this.idJuego = idJuego;
	}
	public int getNoPlayers() {
		return noPlayers;
	}
	public void setNoPlayers(int noPlayers) {
		this.noPlayers = noPlayers;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Long getIdJugador1() {
		return idJugador1;
	}
	public void setIdJugador1(Long idJugador1) {
		this.idJugador1 = idJugador1;
	}
	public Long getIdJugador2() {
		return idJugador2;
	}
	public void setIdJugador2(Long idJugador2) {
		this.idJugador2 = idJugador2;
	}
	public Long getIdJugador3() {
		return idJugador3;
	}
	public void setIdJugador3(Long idJugador3) {
		this.idJugador3 = idJugador3;
	}
	public Long getIdJugador4() {
		return idJugador4;
	}
	public void setIdJugador4(Long idJugador4) {
		this.idJugador4 = idJugador4;
	}
	public Long getIdJugador5() {
		return idJugador5;
	}
	public void setIdJugador5(Long idJugador5) {
		this.idJugador5 = idJugador5;
	}
	public Long getIdJugador6() {
		return idJugador6;
	}
	public void setIdJugador6(Long idJugador6) {
		this.idJugador6 = idJugador6;
	}
	public Long getIdJugador7() {
		return idJugador7;
	}
	public void setIdJugador7(Long idJugador7) {
		this.idJugador7 = idJugador7;
	}
	public Long getIdJugador8() {
		return idJugador8;
	}
	public void setIdJugador8(Long idJugador8) {
		this.idJugador8 = idJugador8;
	}
	public int assignNextPlayer(Long idJugador){
		if(idJugador1 == -1){
			idJugador1 = idJugador;
			return 1; 
		}
		if(idJugador2 == -1){
			idJugador2 = idJugador;
			return 2;
		}
		if(idJugador3 == -1){
			idJugador3 = idJugador;
			return 3;
		}
		if(idJugador4 == -1){
			idJugador4 = idJugador;
			return 4;
		}
		if(idJugador5 == -1){
			idJugador5 = idJugador;
			return 5;
		}
		if(idJugador6 == -1){
			idJugador6 = idJugador;
			return 6;
		}
		if(idJugador7 == -1){
			idJugador7 = idJugador;
			return 7;
		}
		if(idJugador8 == -1){
			idJugador8 = idJugador;
			return 8;
		}
		return -10;
	}
}
