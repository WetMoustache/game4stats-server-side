package es.wetware.game4statisticsserver.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown=true)


public class Player {
    private Long idPlayer;
    private String name;
    private String email;
    private String password;
    private byte[] encodedImage;
    private String gcmId;
    private String token;
    
    /*public Player (Long idPlayer, String name, String password, String email){
    	this.idPlayer = idPlayer;
    	this.name = name;
    	this.password = password;
    	this.email = email;
    }*/
    
    public Player (Long idPlayer, String name, String password, String email, byte[] encodedImage, String token, String gcmid){
    	this.idPlayer = idPlayer;
    	this.name = name;
    	this.password = password;
    	this.email = email;
    	this.encodedImage = encodedImage;
    	this.gcmId = gcmid;
    	this.token = token;
    }
    
    public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Player getUserPlayer(Player player){
    	Player user = new Player();
    	user.setName(player.getName());
    	user.setEncodedImage(player.getEncodedImage());
    	user.setToken(player.getToken());
    	return user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Player(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(Long id) {
        this.idPlayer = id;
    }

    public byte[] getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(byte[] encodedImage) {
        this.encodedImage = encodedImage;
    }


}
