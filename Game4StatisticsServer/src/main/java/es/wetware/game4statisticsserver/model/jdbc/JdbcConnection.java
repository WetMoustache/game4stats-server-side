package es.wetware.game4statisticsserver.model.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnection {
	Connection conexion;

	public JdbcConnection(){
		conexion = null;
	}
	public boolean dbConnect() {
		
		System.out.println("---dbConectar---");
		
		String driver = "org.postgresql.Driver";
		String servidor = "localhost"; // Direccion IP
		String puerto = "5432";
		String database = "gamestatistics";
		String url = "jdbc:postgresql://" + servidor + ":" + puerto + "/" + database;
		String user = "postgres";
		String password = "12345";

		try {

			System.out.println("---Conectando a PostgreSQL---");
			Class.forName(driver);
			conexion = DriverManager.getConnection(url, user, password);
			System.out.println("Conexi�n realizada a la base de datos");
			return true;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean dbDisconnect() {
		System.out.println("---dbDesconectar---");

		try {
			// connection.commit();
			conexion.close();
			System.out.println("Desconexi�n realizada correctamente");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public Connection getConnection(){
		return this.conexion;
	}
}
