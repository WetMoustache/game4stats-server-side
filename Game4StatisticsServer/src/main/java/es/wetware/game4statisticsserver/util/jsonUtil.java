package es.wetware.game4statisticsserver.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import es.wetware.game4statisticsserver.model.Game;
import es.wetware.game4statisticsserver.model.Player;
import es.wetware.game4statisticsserver.model.PlayersForMatch;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class jsonUtil {
	
	
	public static Player jsonToPlayer(InputStream stream) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		Player model = mapper.readValue(stream, Player.class);
		return model;
	}
	
	public static String playerToJson(Player p){
		ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
			json = mapper.writeValueAsString(p);
	        return json;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("unable to parse player to json");
		}        
        return null;
	}
	
	public static OutputStream playersArrayToJson(ArrayList<Player> playerArray) {
		OutputStream out = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(out, playerArray);
			System.out.println(out);
			return out;
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static OutputStream gamesArrayToJson(ArrayList<Game> gameArray) {
		OutputStream out = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(out, gameArray);
			System.out.println(out);
			return out;
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	   public static PlayersForMatch jsonToPlayersForMatch(InputStream stream){
	        ObjectMapper mapper = new ObjectMapper();
	        PlayersForMatch model = null;
	        try {
	            model = mapper.readValue(stream, PlayersForMatch.class);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return model;
	    }

	public static String playersForMatchToJson(PlayersForMatch playersForMatch) {
		ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
			json = mapper.writeValueAsString(playersForMatch);
	        return json;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("unable to parse player to json");
		}        
        return null;
	}
}
